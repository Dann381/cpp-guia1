#include <iostream>
#include <cctype>
using namespace std;

void Clear()
{
    cout << "\x1B[2J\x1B[H";
}

void imprime_vector(string *vec, int n) {
    for (int i=0; i<n; i++){
        int mayus = 0;
        int minus = 0;
        cout << vec[i];
        for (int j=0; j<vec[i].size(); j++){
            //cout << vec[i][j] << endl;
            if (isupper(vec[i][j])){
                mayus = mayus + 1;
            }
            else if (islower(vec[i][j])){
                minus = minus + 1;
            }
        }
        cout << " || mayúsculas: ";
        cout << mayus;
        cout << " || minúsculas: ";
        cout << minus << endl;    
    }
    cout << endl;
}

int main (){
    Clear();
    int n;
    cout << "Ingrese la cantidad de frases que agregará: ";
    cin >> n;
    string vector[n];
    string frase;
    cin.ignore();
    Clear();
    for (int i=0; i<n; i++){
        cout << "Ingrese una frase a continuación: ";
        getline(cin, frase);
        vector[i] = frase;
    }

    imprime_vector(vector, n);

    return 0;
}