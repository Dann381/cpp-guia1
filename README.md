# Guía 1

Los archivos contenidos en este repositorio contienen 3 ejercicios de c++ que están descritos en el pdf adjunto en el mismo repositorio. A continuación se dará una breve explicación de cada ejercicio.

### Ejercicio 1
Este algoritmo consiste en la creación de un arreglo unidimensional con tamaño determinado por el usuario, el cuál es llenado con números enteros que también son ingresados por el usuario, para a continuación obtener el cuadrado de cada número, sumarlos, e imprimir el resultado.

### Ejercicio 2
Este algoritmo consiste en la creación de una lista con datos de tipo "string", cuyo tamaño es determinado por el usuario. El algoritmo recibe frases con mayúsculas y minúsculas, las cuáles son contadas para al final del programa imprimir cuántas mayúsculas y minúsculas tiene cada frase ingresada.

### Ejercicio 3
Este último algoritmo contiene un archivo Main, en el cuál se escribió el menú y por dónde se reciben los datos ingresados. Además contiene un archivo ".cpp" con su respectivo ".h", en el cuál se construyó un objeto persona que tiene atributos como "nombre", "telefono", "saldo", y un atributo de tipo booleano para determinar si la persona es morosa o no. Al ejecutar el programa, se muestra un menú por la terminal que le permite al usuario ingresar los datos de una persona, mostrar todos los datos almacenados, o terminar el programa.

Cada ejercicio cuenta con su respectivo makefile para poder compilar los algoritmos de forma más amigable.

### Construido con
El presente proyecto de programación, se construyó en base al lenguaje de programación C++, en conjunto con el IDE "Visual Studio Code".

* [C++] - Lenguaje de programación utilizado.
* [Visual Studio Code](https://code.visualstudio.com/) - IDE utilizado.

### Autores
* **Daniel Tobar** - Estudiante de Ingeniería Civil en Bioinformática.
