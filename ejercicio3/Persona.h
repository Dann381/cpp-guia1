#ifndef PERSONA_H
#define PERSONA_H

class Persona{
    private:
        string nombre = "\0";
        string telefono = "\0";
        int saldo = 0;
        bool moroso = false;
    public:
        Persona();
        Persona(string nombre, string telefono, int  saldo, bool moroso);

        void set_nombre(string nombre);
        void set_telefono(string telefono);
        void set_saldo(int saldo);
        void set_moroso(bool moroso);
        string get_nombre();
        string get_telefono();
        int get_saldo();
        bool get_moroso();
};
#endif