#include <iostream>
using namespace std;
#include "Persona.h"

Persona::Persona(){
    string nombre = "\0";
    string telefono = "\0";
    int saldo = 0;
    bool moroso = false;
}

Persona::Persona(string nombre, string telefono, int saldo, bool moroso){
    this->nombre = nombre;
    this->telefono = telefono;
    this->saldo = saldo;
    this->moroso = moroso; 
}

void Persona::set_nombre(string nombre){
    this->nombre = nombre;
}

void Persona::set_telefono(string telefono){
    this->telefono = telefono;
}

void Persona::set_saldo(int saldo){
    this->saldo = saldo;
}

void Persona::set_moroso(bool moroso){
    this->moroso = moroso;
}

string Persona::get_nombre(){
    return this->nombre;
}

string Persona::get_telefono(){
    return this->telefono;
}

int Persona::get_saldo(){
    return this->saldo;
}

bool Persona::get_moroso(){
    return this->moroso;
}