#include <list>
#include <iostream>
using namespace std;
#include "Persona.h"

void Clear()
{
    cout << "\x1B[2J\x1B[H";
}

int main(){

    //Creación de la persona inicial
    list<Persona> personas;
    Persona persona1 = Persona("Daniel", "982616845", 10000, false);
    personas.push_back(persona1);
    cout << persona1.get_nombre() << endl;
    cout << persona1.get_telefono() << endl;
    cout << persona1.get_saldo() << endl;
    cout << persona1.get_moroso() << endl;

    //inicio del menú
    int opcion = 0;
    int salir = 0;
    Clear();
    while (salir != 1){
        cout << "- Opciones -" << endl;
        cout << "\n1.- Ingresar persona" << endl;
        cout << "2.- Imprimir datos" << endl;
        cout << "3.- Salir" << endl;
        cout << "\nIngese opción: ";
        cin >> opcion;
        Clear();
        if (opcion == 1){
            string nombre;
            string telefono;
            int saldo;
            bool moroso;
            cout << "Ingrese nombre: ";
            cin >> nombre;
            cout << "Ingrese telefono: ";
            cin >> telefono;
            cout << "Ingrese saldo: ";
            cin >> saldo;
            cout << "¿La persona está morosa? (0 para NO, 1 para SI): ";
            cin >> moroso;
            Persona persona = Persona(nombre, telefono, saldo, moroso);
            personas.push_back(persona);
            Clear();
            cout << "Persona ingresada correctamente" << endl;
        }
        else if (opcion == 2){
            Clear();
            cout << "Mostrando datos de todas las personas ingresadas:\n" << endl;

            for (Persona persona : personas){
                cout << "Nombre: " + persona.get_nombre() << endl;
                cout << "Teléfono: " + persona.get_telefono() << endl;
                cout << "Saldo: ";
                cout << persona.get_saldo() << endl;
                cout << "Moroso (0 = NO, 1 = SI): ";
                cout << persona.get_moroso() << endl;
                cout << endl;
            }
        }
        else if (opcion == 3){
            Clear();
            cout << "Saliendo..." << endl;
            salir = 1;
        }
        else {
            Clear();
            cout << "Ingrese un número válido" << endl;
        }
    } 





    return 0;
}