#include <iostream>
using namespace std;

void Clear()
{
    cout << "\x1B[2J\x1B[H";
}

void imprime_vector(int *vec, int n) {
    int suma = 0;
    cout << "Los números ingresados fueron: ";
    for (int i=0; i<n; i++){
        suma = suma + (vec[i]*vec[i]);
        cout << vec[i] << " ";    
    }
    cout << endl;
    cout << "La suma de los cuadrados de los números es: ";
    cout << suma << endl;
}

int main (){
    Clear();
    int n;
    cout << "Ingrese el tamaño de la lista: ";
    cin >> n;

    int vector1[n];
    Clear();
    for (int i=0; i<n; i++){
        int num;
        cout << "Ingrese los números a continuación: ";
        cin >> num;
        vector1[i] = num;
    }

    imprime_vector(vector1, n);

    return 0;
}
